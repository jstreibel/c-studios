//
// Created by joao on 22/07/2021.
//

#ifndef ISING_MANYHISTORIES_H
#define ISING_MANYHISTORIES_H


#include "IsingMonteCarlo/IsingMonteCarloCalculator.h"

#include <iostream>
#include <string>


int RunManyHistories(int argc, char **argv){
    if(argc != 5){
        throw "Wrong argc. Use ./Ising [L] [MCSteps] [transient] [T]";
    }

    int L = atoi(argv[1]);
    int MCSteps = atoi(argv[2]);
    int transient = atoi(argv[3]);
    double T = atof(argv[4]);

    try {
        //std::cout << "#1   2    3   4    5    6   7    8    9\n";
        //std::cout << "#T   e   e2   m   m2   m4   Cv   Xi   B\n";

        auto *vc = new ThermoOutput::ViewController(L, MCSteps, transient);

        IsingMonteCarloCalculator mcCalculator(L, MCSteps, transient,
                                               IsingMonteCarloCalculator::Ferromagnetic,
                                               IsingMonteCarloCalculator::Metropolis,
                                               vc);

        mcCalculator.Simulate(T, 0, MCSteps, transient);

        std::cout << "\n\n";
    } catch(const char* exception) {
        std::cout << "Ising exception: " << exception << "\n";
    }

    std::cout << std::endl;

    return 0;
}


#endif //ISING_MANYHISTORIES_H
