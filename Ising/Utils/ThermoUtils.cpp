//
// Created by joao on 16/05/2021.
//

#include "ThermoUtils.h"

#include <cmath>

namespace ThermoUtils {
    Real f_Gibbs(Real T, Real Z, Real N) { return -T * log(Z) / N; }

    Real c_v(Real T, Real e_av, Real e2_av, Real N) {
        return N * (e2_av - e_av * e_av) / (T * T);
    }

    Real chi(Real T, Real m_av, Real m2_av, Real N) { return N * (m2_av - m_av * m_av) / T; }

    Real BoltzmannWeight_betaE(Real betaE) { return exp(-betaE); }

    Real BoltzmannWeight(Real T, Real E) { return exp(-E / T); }


}
