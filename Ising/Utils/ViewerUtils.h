//
// Created by joao on 18/05/2021.
//

#ifndef ISING_VIEWERUTILS_H
#define ISING_VIEWERUTILS_H

#include <SFML/Graphics/Font.hpp>

namespace ViewerUtils {


    void fontMeUp(){
        if (!font.loadFromFile("../Data/Fonts/static/EBGaramond-Regular.ttf"))
            throw "SFML error.";
        text = sf::Text("Ising", font, fontSize);
        text.setPosition(border, isingSpriteSize + border);
    }



}

#endif //ISING_VIEWERUTILS_H
