set xlabel "T"
set ylabel "<m^4>"
set title "<m^4> metropolis"

plot "MCIsing5x5.dat" using 1:6 title "5x5" with lines, \
     "MCIsing10x10.dat" using 1:6 title "10x10" with lines, \
     "MCIsing20x20.dat" using 1:6 title "20x20" with lines