set xlabel "T"
set ylabel "Cv"
set title "Cv"

plot "Ising5x5-n=180.dat" using 1:5 title "Exato 5x5" with points ps 0.5, \
     "MCIsing5x5.dat" using 1:7 title "MC 5x5" with lines