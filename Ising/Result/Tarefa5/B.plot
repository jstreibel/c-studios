set xlabel "T"
set ylabel "B(T)"
set title "B(T) metropolis"

#set arrow 3 from 2.269,0 to 2.269,1 nohead lc rgb 'red';

plot "MCIsing5x5.dat" using 1:9 title "5x5" with lines, \
     "MCIsing10x10.dat" using 1:9 title "10x10" with lines, \
     "MCIsing15x15.dat" using 1:9 title "15x15" with lines, \
     "MCIsing20x20.dat" using 1:9 title "20x20" with lines, \
     "Tc.dat" using 1:2 title "Tc" with lines lc rgb 'red'