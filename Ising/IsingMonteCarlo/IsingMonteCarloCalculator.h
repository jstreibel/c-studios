//
// Created by joao on 15/05/2021.
//

#ifndef ISING_ISINGMONTECARLOCALCULATOR_H
#define ISING_ISINGMONTECARLOCALCULATOR_H

#include "IsingNetwork.h"
#include "../Utils/ThermoUtils.h"
#include "IOProgram/ViewController.h"


typedef std::vector<ThermoUtils::Real> RealVector;


class IsingMonteCarloCalculator {

public:
    enum InitialConditions {
        Ferromagnetic,
        Paramagnetic
    };

    enum Dynamic {
        Metropolis,
        Kawasaki
    };

private:
    InitialConditions ic;
    Dynamic aDynamic;

    IsingNetwork S;

    bool __shouldAccept(ThermoUtils::Real deltaE, ThermoUtils::Real T) const;

    void _MCStep(ThermoUtils::Real T, ThermoUtils::Real h, int N);

    void __MCStepMetropolis(ThermoUtils::Real T, ThermoUtils::Real h, int N);
    void __MCStepKawasaki(ThermoUtils::Real T, ThermoUtils::Real h, int N);

    void _shake(double h);
public:

    explicit IsingMonteCarloCalculator(int L, int MCSteps, int transientSize, InitialConditions ic, Dynamic dynamic, ThermoOutput::ViewControlBase *viewer);

    ~IsingMonteCarloCalculator() = default;

    void Simulate(ThermoUtils::Real T, ThermoUtils::Real h, int MCSteps, int transientSize);

private:
    ThermoOutput::ViewControlBase *vcOutput;

    static void
    _outputDataToConsole(const RealVector &e, const RealVector &e2, const RealVector &m, const RealVector &m2,
                         const RealVector &m4, long double T, double N);
};

#endif //ISING_ISINGMONTECARLOCALCULATOR_H
