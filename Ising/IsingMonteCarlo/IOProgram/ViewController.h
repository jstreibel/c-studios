//
// Created by joao on 17/05/2021.
//

#ifndef ISING_VIEWCONTROLLER_H
#define ISING_VIEWCONTROLLER_H

#include <SFML/Graphics.hpp>
#include "../IsingNetwork.h"
#include "../../Utils/ThermoUtils.h"
#include "GraphAndAverageCalc.h"
#include "ViewControlBase.h"


namespace ThermoOutput {
    const double T_c = 2.269;

    class ViewController : public ViewControlBase {
        sf::RenderWindow window;
        sf::Font font;
        sf::Text text;

        sf::Image IsingBitmap;
        sf::Texture IsingTexture;
        sf::Sprite IsingSprite;

        GraphAndAverageCalc *mag_t_View,
                            *en_t_View;

        Graph *corr_t_View,
              *T_t_View,
              *h_t_View,
              *mag_T_View,
              *en_T_View,
              *chi_T_View,
              *C_v_View,
              *histeresisView;
    public:
        explicit ViewController(int L, int MCSteps, int transientSize);
        ~ViewController();

        bool doOperate(SystemParams &params, OutputData &data);

        void hide();

    private:
        void _updateGraphsAndData(SystemParams &params, OutputData &data);
        void _updateIsingGraph(const IsingNetwork &S);
        void _treatEvents(SystemParams &params, OutputData &data);

        sf::Clock timer;
        virtual void _runIOProgram(SystemParams &params, OutputData &data);
        void __manipulationOfParametersHasHappened(double newValue, double lastValue, double N);


        void _drawEverything(SystemParams &params, OutputData &data);


        void __outputHistoryToFile(int L);
        void __outputAveragesHistoryToFile(int L);
        void __outputHisteresisToFile(int L);
    };
}


#endif //ISING_VIEWCONTROLLER_H
