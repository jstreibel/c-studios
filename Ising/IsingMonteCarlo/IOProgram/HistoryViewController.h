//
// Created by joao on 22/07/2021.
//

#ifndef ISING_HISTORYVIEWCONTROLLER_H
#define ISING_HISTORYVIEWCONTROLLER_H


#include "ViewControlBase.h"


namespace ThermoOutput {

    class HistoryViewController : ViewControlBase {
        bool doOperate(SystemParams &params, OutputData &data) override;
    };
}


#endif //ISING_HISTORYVIEWCONTROLLER_H
