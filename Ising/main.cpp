

#include "BasicSim.h"
#include "ManyHistories.h"


int main(int argc, char *argv[]) {

    try {
        if (1) RunManyHistories(argc, argv);
        else return RunBasicSim(argc, argv);
    } catch (const char *msg){
        std::cout << "Exception: \"" << msg << "\". Terminating";
    }
}
